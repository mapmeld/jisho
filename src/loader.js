'use strict';

const path = require('path');

const debug = require('debug')('jisho:loader');

module.exports = function(name) {
  const filepath = path.resolve(__dirname, '..', 'data', `${name}.json`);
  debug(`Loading ${filepath}`);

  return require(filepath);
};
