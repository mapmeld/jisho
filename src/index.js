#!/usr/bin/env node

'use strict';

const path = require('path');

const debug = require('debug')('jisho:main'),
      chalk = require('chalk'),
      meow = require('meow');

const loader = require(path.resolve(__dirname, 'loader.js')),
      kanjis = loader('kanjidict'),
      // conjug = loader('conjugations'),
      elements = loader('elements');

const cli = meow(`
  Usage
    $ jisho <input>

  Options
    -e, --examples  Show examples (can be a long list !)

  Examples
    $ jisho 冥
    Onyomi メイ(漢)、ミョウ(呉)
    Kunyomi くら（い）
    Strokes 10
    Meaning dark, Hades
    JLPT N1 (advanced)
    Parts 冖,日,八,亠
    Part of 幎,暝,榠,溟,瞑,螟
`, {
  alias: {
    e: 'examples'
  }
});

if (cli.input) {
  debug('cli', cli);

  const input = cli.input[0],
        flags = cli.flags;

  var result = getKanjiInfo(input);

  if (result) {

    result.onyomi && log('Onyomi', result.onyomi.join('、'));
    result.kunyomi && log('Kunyomi', result.kunyomi);
    result.strokes && log('Strokes', result.strokes);
    result.meaning && log('Meaning', result.meaning.join(', '));
    result.jlpt && log('JLPT', result.jlpt);
    result.kanji_parts && log('Parts', result.kanji_parts.join(', '));
    result.part_of && log('Part of', result.part_of.join(', '));
  }

  flags.examples && showExamples(input);

} else {
  console.log('Please give jisho something to search !');
  process.exit(0);
}

function getKanjiInfo(input) {
  if (!input) {
    return null;
  }

  var kanjiData = kanjis.filter(x => x.kanji === input)[0],
      elementsData = elements.filter(x => x.kanji === input)[0],
      result = Object.assign({}, kanjiData, elementsData);

  if (!result || !Object.keys(result).length) {
    return null;
  }

  result.onyomi = (result.onyomi || '').split('、');
  result.meaning = (result.meaning || '').split(';');
  result.kanji_parts = (result.kanji_parts || '').split(',');
  result.part_of = (result.part_of || '').split(',');

  debug('Kanji data', result);
  return result;
}

function log(text, data) {
  console.log(chalk.inverse(text), data);
}

function showExamples(input) {
  const sentences = loader('sentences');
  const examplesData = sentences.filter(x => x.kanji.includes(input));

  if (examplesData.length) {
    console.log(chalk.inverse('Examples'));
    examplesData.forEach(x => console.log(`- ${x.japanese} (${x.english})`));
  } else {
    console.log(chalk.red('No examples available'));
  }
}

module.exports = getKanjiInfo;
