const assert = require('assert');
const jisho = require('../src/index.js');

describe('Use as a module', function() {
  it('should return information about 冥 kanji', function() {
    var result = jisho('冥');

    assert.deepEqual(result.onyomi, ['メイ(漢)', 'ミョウ(呉)']);
    assert.equal(result.kunyomi, 'くら（い）');
    assert.equal(result.strokes, 10);
    assert.deepEqual(result.meaning, ['dark', 'Hades']);
    assert.equal(result.jlpt, 'N1 (advanced)');
    assert.deepEqual(result.kanji_parts, ['冖','日','八','亠']);
    assert.deepEqual(result.part_of, ['幎','暝','榠','溟','瞑','螟']);
  });

  it('should handle non-kanji input', function() {
    assert.equal(jisho('x'), null);
  });
});
