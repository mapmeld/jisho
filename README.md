# Jisho

A command-line Japanese kanji dictionary.

Uses data from [kanjium](https://github.com/mifunetoshiro/kanjium) ([license](data/license.txt)).

Not affiliated with the amazing [jisho](http://jisho.org).

## Install

```
npm install -g jisho
```

## Usage

```
jisho [kanji]
```

For example:

```
> jisho 冥

Onyomi メイ(漢)、ミョウ(呉)
Kunyomi くら（い）
Strokes 10
Meaning dark, Hades
JLPT N1 (advanced)
Parts 冖,日,八,亠
Part of 幎,暝,榠,溟,瞑,螟
```

### Options

- `--examples` or `-e` to show examples (can be a long list !)

## TODO

- Add conjugation for verbs/adjectives
- Add search from radicals

## License

MIT © [Siegfried Ehret](http://ehret.me)
